package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
)

func main() {
	// Retrieve the filename of the config file from the command-line
	var fn string
	flag.StringVar(&fn, "config", "config.json", "path to the configuration file")
	flag.Parse()

	// Create a watcher and restarter
	w, err := NewWatcher(fn)
	if err != nil {
		log.Fatal(err)
	}
	r, err := NewRestarter()
	if err != nil {
		log.Fatal(err)
	}

	// Create a channel and run the watcher & restarter
	svcs := make(chan string)
	go w.Watch(svcs)
	go r.Restart(svcs)

	// Log that the daemon started without error
	log.Println("Watching keys for changes...")

	// Wait for a Ctrl+C (more specifically, a SIGINT)
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
}
