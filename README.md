## Etcd Monitor

A convenient tool for monitoring etcd keys and restarting a predefined list of services when one of them changes. Etcd Monitor is written in Go.

### Configuration

The configuration is stored in a JSON file with the following format:

    {
        "/test/trigger": ["dummy@.service"]
    }

Basically, the file consists of a map of etcd keys that describe which services should be restarted when that specific key receives a new value

When specifying services, both the `xxx.service` and `xxx@.service` formats are accepted. The latter will cause all units using the template to restart.

### Usage

Currently, the application accepts a single optional argument:

    etcdmon -config /path/to/config.json

By default, it is assumed that a file named `config.json` exists in the current working directory.
