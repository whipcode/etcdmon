package main

import (
	"github.com/coreos/fleet/client"
	"net"
	"net/http"
)

type Fleet struct {
	api client.API
}

// Create a new Fleet client
func NewFleet() (*Fleet, error) {
	cl := &http.Client{
		Transport: &http.Transport{
			Dial: func(string, string) (net.Conn, error) {
				return net.Dial("unix", "/var/run/fleet.sock")
			},
		},
	}
	c, err := client.NewHTTPClient(cl, "http://localhost/")
	if err != nil {
		return nil, err
	}
	return &Fleet{c}, nil
}

// Stop the specified unit
func (f Fleet) Stop(unit string) error {
	return f.api.SetUnitTargetState(unit, "loaded")
}

// Start the specified unit
func (f Fleet) Start(unit string) error {
	return f.api.SetUnitTargetState(unit, "launched")
}

// List all units by name
func (f Fleet) List() ([]string, error) {
	states, err := f.api.UnitStates()
	if err != nil {
		return nil, err
	}
	names := make([]string, len(states))
	for i, s := range states {
		names[i] = s.Name
	}
	return names, nil
}
