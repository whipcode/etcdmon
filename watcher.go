package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"
)

type Watcher struct {
	keys  map[string][]string
	index uint64
}

type response struct {
	Action string `json:"action"`
	Node   struct {
		Key           string `json:"key"`
		ModifiedIndex uint64 `json:"modified_index"`
	} `json:"node"`
}

// Create a watcher by loading the keys from the specified file
func NewWatcher(fn string) (*Watcher, error) {
	b, err := ioutil.ReadFile(fn)
	if err != nil {
		return nil, err
	}
	w := &Watcher{}
	if err := json.Unmarshal(b, &w.keys); err != nil {
		return nil, err
	}
	return w, nil
}

// Read from the specified io.Reader
func (w Watcher) read(r io.Reader, wc chan<- []byte, ec chan<- error) {
	b, err := ioutil.ReadAll(r)
	if err == nil {
		wc <- b
	} else {
		ec <- err
	}
}

// Watch the root key for changes up to a maximum of one minute
func (w Watcher) poll() ([]byte, error) {
	for {
		v := url.Values{}
		v.Set("consistent", "true")
		v.Set("modifiedIndex", fmt.Sprintf("%d", w.index))
		v.Set("recursive", "true")
		v.Set("wait", "true")
		u := url.URL{
			Scheme:   "http",
			Host:     "localhost:4001",
			Path:     "/v2/keys/",
			RawQuery: v.Encode(),
		}
		resp, err := http.Get(u.String())
		if err != nil {
			return nil, err
		}
		defer resp.Body.Close()
		wc := make(chan []byte)
		ec := make(chan error)
		go w.read(resp.Body, wc, ec)
		select {
		case b := <-wc:
			return b, nil
		case e := <-ec:
			return nil, e
		case <-time.After(time.Minute):
			resp.Body.Close()
		}
	}
}

// Watch keys for changes, sending services to restart on the channel
func (w *Watcher) Watch(svcs chan<- string) {
	for {
		b, err := w.poll()
		if err != nil {
			log.Fatal(err)
		}
		var r = &response{}
		if err := json.Unmarshal(b, &r); err != nil {
			log.Fatal(err)
		}
		w.index = r.Node.ModifiedIndex + 1
		if v, ok := w.keys[r.Node.Key]; ok && r.Action == "set" {
			log.Printf("Key %s [%s]...\n", r.Node.Key, r.Action)
			for _, s := range v {
				svcs <- s
			}
		}
	}
}
