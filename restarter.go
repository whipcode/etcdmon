package main

import (
	"log"
	"regexp"
	"strings"
	"time"
)

type Restarter struct {
	fleet *Fleet
}

// Create a new restarter
func NewRestarter() (*Restarter, error) {
	f, err := NewFleet()
	if err != nil {
		return nil, err
	}
	r := &Restarter{
		fleet: f,
	}
	return r, nil
}

/**
 * In order to ensure that each operation is properly acted on, the Sleep calls
 * below must be made. In my experience, failing to "spread out" the calls
 * results in absolutely nothing happening.
 */

// Restart the specified service
func (r Restarter) restartService(unit string) error {
	log.Printf("Restarting %s...\n", unit)
	if err := r.fleet.Stop(unit); err != nil {
		return err
	}
	time.Sleep(2 * time.Second)
	if err := r.fleet.Start(unit); err != nil {
		return err
	}
	time.Sleep(2 * time.Second)
	return nil
}

// Used for extracting parts of a template
var templ = regexp.MustCompile("^(.+)@(.+).service$")

// Restart all units for a given template
func (r Restarter) restartTemplate(svc string) error {
	n := strings.TrimSuffix(svc, "@.service")
	units, err := r.fleet.List()
	if err != nil {
		return err
	}
	for _, u := range units {
		m := templ.FindStringSubmatch(u)
		if m != nil && m[1] == n {
			r.restartService(u)
		}
	}
	return nil
}

// Restart services as requested
func (r Restarter) Restart(svcs <-chan string) {
	for s := range svcs {
		if strings.HasSuffix(s, "@.service") {
			if err := r.restartTemplate(s); err != nil {
				log.Fatal(err)
			}
		} else {
			if err := r.restartService(s); err != nil {
				log.Fatal(err)
			}
		}
	}
}
